-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 30-03-2019 a las 01:24:11
-- Versión del servidor: 10.1.26-MariaDB
-- Versión de PHP: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `businesssimulator`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tablaprincipal`
--

CREATE TABLE `tablaprincipal` (
  `id` int(10) NOT NULL,
  `NombreUsuario` varchar(25) NOT NULL,
  `ApellidoUsuario` varchar(25) NOT NULL,
  `NombreEmpresa` varchar(50) NOT NULL,
  `CP` int(20) NOT NULL,
  `NumeroTrabajadores` int(10) NOT NULL,
  `GastoMensual` int(20) NOT NULL,
  `GananciaMensual` int(20) NOT NULL,
  `PerdidaMensual` int(11) NOT NULL,
  `Pass` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tablaprincipal`
--

INSERT INTO `tablaprincipal` (`id`, `NombreUsuario`, `ApellidoUsuario`, `NombreEmpresa`, `CP`, `NumeroTrabajadores`, `GastoMensual`, `GananciaMensual`, `PerdidaMensual`, `Pass`) VALUES
(1, 'David', 'Villa', 'Developers.yea', 0, 0, 0, 0, 0, '123');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tablaprincipal`
--
ALTER TABLE `tablaprincipal`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tablaprincipal`
--
ALTER TABLE `tablaprincipal`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
